(function ($) {
  Drupal.behaviors.common_helpers = {
    generateRandomBuffer: (length) => {
      if (!length) length = 32;

      var randomBuff = new Uint8Array(length);
      window.crypto.getRandomValues(randomBuff);
      return randomBuff;
    },
  };

  $.fn.notification_callback = function (msg) {
    alert(msg)
  }

  $.fn.webauthn_register = function (username) {
    var payload = {};
    var displayName = username;

    payload.id = base64url.encode(
      Drupal.behaviors.common_helpers.generateRandomBuffer(32)
    );
    payload.credentials = [];

    var publicKey = {
      challenge: payload.id,
      rp: {
        name: "Carbonpay",
      },
      user: {
        id: payload.id,
        name: username,
        displayName: displayName,
      },
      pubKeyCredParams: [
        { type: "public-key", alg: -7 },
        { type: "public-key", alg: -35 },
        { type: "public-key", alg: -36 },
        { type: "public-key", alg: -257 },
        { type: "public-key", alg: -258 },
        { type: "public-key", alg: -259 },
        { type: "public-key", alg: -37 },
        { type: "public-key", alg: -38 },
        { type: "public-key", alg: -39 },
        { type: "public-key", alg: -8 },
      ],
      status: "ok",
    };

    var options = {};
    options.uv = true;

    if (options) {
      if (!publicKey.authenticatorSelection)
        publicKey.authenticatorSelection = {};

      if (options.attestation) {
        publicKey.attestation = options.attestation;
      } else {
        publicKey.attestation = options.attestation;
      }

      if (options.rpId) publicKey.rp.id = options.rpId;

      if (options.uv)
        publicKey.authenticatorSelection.userVerification = "required";
    }

    if (!publicKey.authenticatorSelection)
      publicKey.authenticatorSelection = {};

    publicKey.challenge = base64url.decode(payload.id);
    publicKey.user = {};
    publicKey.user.id = base64url.decode(payload.id);
    publicKey.user.name = username;
    publicKey.user.displayName = displayName;
    publicKey.success = true;

    return navigator.credentials
      .create({ publicKey: publicKey })
      .then(function (creds) {
        $.ajax({
          type: "POST",
          url: "/api/webauthn/register-callback",
          data: {
            id: payload.id,
            nav_cred_res: creds.id,
            username: username,
          },
          success: function (data) {
            var jsonResponse = JSON.parse(data);
            if (jsonResponse.response) {
              window.location.href = "/user";
            } else {
              alert("something went wrong");
            }
          },
          error: function (error) {
            $.fn.notification_callback(error);
          },
        });
      });
  };

  $.fn.webauthn_login = function (username, navigator_response_id) {
    var session = {};
    session.username = username;
    session.uv = true;

    var token = base64url.encode(
      Drupal.behaviors.common_helpers.generateRandomBuffer(32)
    );

    session.challenge = token;
    var publicKey = {
      challenge: session.challenge,
      status: "ok",
    };

    if (session.username) {
      publicKey.allowCredentials = [
        { type: "public-key", id: navigator_response_id },
      ];
    }

    if (session.uv) {
      publicKey.userVerification = "required";
    }

    const getAssertionChallenge = publicKey;
    getAssertionChallenge.challenge = base64url.decode(
      getAssertionChallenge.challenge
    );

    if (getAssertionChallenge.allowCredentials) {
      for (let allowCred of getAssertionChallenge.allowCredentials) {
        allowCred.id = base64url.decode(allowCred.id);
      }
    }

    return navigator.credentials
      .get({ publicKey: getAssertionChallenge })
      .then(function (creds) {
        $.ajax({
          type: "POST",
          url: "/api/webauthn/login-callback",
          data: {
            username: username,
          },
          success: function (data) {
            var jsonResponse = JSON.parse(data);
            if (jsonResponse.response) {
              window.location.href = '/user';
            } else {
              $.fn.notification_callback("something went wrong");
            }
          },
          error: function (error) {
            $.fn.notification_callback(error);
          },
        });
      });
  };
})(jQuery);
